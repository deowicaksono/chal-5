const express = require('express');
const app= express();
const port = 8000;
const morgan = require('morgan');
const fs = require('fs');

app.set('view engine','ejs');
app.use(express.urlencoded({extended: true}))
app.use(express.json());
app.use(express.static('public'))

const log = morgan(':method :url - :response-time ms')
let routeGames = require('./routes/games');
let routeLogin = require('./routes/login.js');
let routeRegister= require('./routes/register.js')

let dataUser=require('./users.json');
console.log(dataUser)


app.get('/', log, (req,res) =>{
    res.status(200).render('index')
})

app.use(routeGames);
app.use(routeLogin);
// nyoba bikin register, tapi gagal di writefilesync bentuk objek
// app.use(routeRegister);


app.get('/getloginjson', (req,res)=> {
    res.status(200).json(dataUser);
})







app.get('*', log, (req,res)=>{
    res.status(404).send("page doesnt exist");
})





app.listen(port, () => console.log('app currently listening'));