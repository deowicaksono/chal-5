var express=require('express');
const morgan = require('morgan');
var router = express.Router();
const log = morgan(':method :url - :response-time ms')

router.get('/game',log, (req,res)=>{
    res.status(200).render('game')
})

module.exports=router;