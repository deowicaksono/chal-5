var express=require('express');
const morgan = require('morgan');
var router = express.Router();
const log = morgan(':method :url - :response-time ms')
let dataUser=require('../users.json');
router.get('/login',log, (req,res) =>{
    res.render('login');
})

router.post('/login',log, (req,res)=>{
    const {username, password} = req.body;
    console.log(`ini username:` + username);
    console.log('ini password:' + password);
    console.log('Initiate find data user');
    const userFound=dataUser.find(item =>((item.username==username) && (item.password==password)));
    console.log(userFound);
    if(!userFound){
        console.log("user not found, redirect to login page");
        res.redirect('/login')
    }
    else{
        console.log("Hello "+username+" Welcome to our page, redirect to homepage");
        res.redirect('/')
    }
})

module.exports=router;